class Match < ActiveRecord::Base
  belongs_to :matchable, polymorphic: true
end
