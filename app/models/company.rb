class Company < ActiveRecord::Base
  has_many :matches, as: :matchable
end
