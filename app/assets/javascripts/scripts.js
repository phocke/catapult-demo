$(document).ready(function () {

    $('select').dropkick({startSpeed: 0});

    function setOpportunityHeight() {
        var container = $('#profile').find('.opportunities');
        var item = container.find('.active');

        container.css('height', item.outerHeight())
    }

    setOpportunityHeight();

	if( $('#welcome-person').length ) {
	    $('#welcome-person').removeClass('initial');
    }

    $(".knob").knob({
        readOnly: true,
        width: 71,
        thickness: 0.03,
        fgColor: '#0c63e5',
        max: 100,
        font: 'helvetica_neuelight',
        inputColor: '#393c43',
        'draw' : function () {
            $(this.i).val(this.cv + '%')
        }
    });

    $(".knob-with-logo").knob({
        readOnly: true,
        width: 88,
        thickness: 0.03,
        fgColor: '#0c63e5',
        max: 100,
        font: 'helvetica_neuelight',
        inputColor: '#393c43',
        displayInput: false
    });

    $('.dial').each(function () {

        var $this = $(this);
        var myVal = $this.val();
        $this.knob({});

        $({
            value: 0
        }).animate({
            value: myVal
        }, {
            duration: 2000,
            easing: 'swing',
            step: function () {
                $this.val(Math.ceil(this.value)).trigger('change');
            }
        })
    });

    $(".opportunities > li").swipe( {
        swipeLeft:function(event, direction, distance, duration, fingerCount) {

            setOpportunityHeight();

            if ($(this).hasClass('no-finds')) {
                return
            }

            var activeEl = $('.opportunities > li.active');
            var nextEl = $(".opportunities > li.active").next();
            if(nextEl.length==0) {
                nextEl = $('.opportunities > li:first-child');
            }

            $(activeEl).fadeOut(500, function() {
                $(this).removeClass('active')
            });
            $(nextEl).fadeIn(500, function() {
                $(this).addClass('active')
            });

            if ($(this).hasClass('no-finds')) {
                return
            }
        },
        swipeRight:function(event, direction, distance, duration, fingerCount) {

            setOpportunityHeight();

            if ($(this).hasClass('no-finds')) {
                return
            }

            var activeEl = $('.opportunities > li.active');
            var nextEl = $(".opportunities > li.active").next();
            if(nextEl.length==0) {
                nextEl = $('.opportunities > li:first-child');
            }

            $(activeEl).fadeOut(500, function() {
                $(this).removeClass('active')
            });
            $(nextEl).fadeIn(500, function() {
                $(this).addClass('active')
            });
        },
        threshold: 30
    });

    /////////////////////////
    //    DEMO PURPOSE ONLY
    /////////////////////////

    $('#new-user .submit').on('click', function(e) {
        e.preventDefault();
        window.location.href = 'login.html'
    })

    $('#login .submit').on('click', function(e) {
        e.preventDefault();
        window.location.href = 'welcome_person.html'
    })

    $('#welcome-person').find('.questions').find('p').click(function() {

        var activeEl = $('.questions > li.active');
        var nextEl = $(".questions > li.active").next();

        if(nextEl.length==0) {
            window.location.href = 'profile.html'
        }

        $(activeEl).fadeOut(500, function() {
            $(this).removeClass('active')
        });
        $(nextEl).fadeIn(800, function() {
            $(this).addClass('active')

            var currentPagin = $('.pagin-indicator > li.active');
            currentPagin.removeClass('active').next().addClass('active');

        });
    })
});
