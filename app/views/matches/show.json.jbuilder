json.extract! @match, :id, :matchable_type, :matchable_id, :image_source, :percent_match, :job_title, :match_point_1, :match_point_2, :created_at, :updated_at
