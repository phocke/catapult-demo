json.array!(@users) do |user|
  json.extract! user, :id, :user_type, :email, :password, :full_name, :most_recent_job_title, :ideal_job_title_current, :most_recent_company, :ideal_salary, :education_cap, :years_experience, :native_language
  json.url user_url(user, format: :json)
end
