json.array!(@companies) do |company|
  json.extract! company, :id, :organization, :hiring, :expected_salary, :ideal_degree, :ideal_poach_company, :looking_for_a, :with, :native_languages
  json.url company_url(company, format: :json)
end
