module ApplicationHelper

  def profile_data
    defaults = ['id', 'created_at', 'updated_at', 'password', 'user_type', 'email']

    Hash.new.tap do |hash|
      [@profile.attribute_names - defaults].flatten.each do |attribute|
        hash[attribute] = @profile.send(attribute.to_sym)
      end
    end
  end
end
