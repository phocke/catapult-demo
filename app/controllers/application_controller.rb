class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.

  before_filter :set_profile

  def demo
  end

  def attention
  end

  def index
  end

  def login
  end

  def new_user
  end

  def opportunities
  end

  def welcome_person
  end

  def welcome_company
  end

  def profile
  end



private
  def set_profile
    @profile = if params[:profile] == 'user'
      User.first
    elsif params[:profile] == 'company'
      Company.first
    else
      User.first
    end
  end

  protect_from_forgery with: :exception
end
