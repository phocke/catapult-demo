require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  setup do
    @user = users(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:users)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create user" do
    assert_difference('User.count') do
      post :create, user: { education_cap: @user.education_cap, email: @user.email, full_name: @user.full_name, ideal_job_title_current: @user.ideal_job_title_current, ideal_salary: @user.ideal_salary, most_recent_company: @user.most_recent_company, most_recent_job_title: @user.most_recent_job_title, native_language: @user.native_language, password: @user.password, user_type: @user.user_type, years_experience: @user.years_experience }
    end

    assert_redirected_to user_path(assigns(:user))
  end

  test "should show user" do
    get :show, id: @user
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @user
    assert_response :success
  end

  test "should update user" do
    patch :update, id: @user, user: { education_cap: @user.education_cap, email: @user.email, full_name: @user.full_name, ideal_job_title_current: @user.ideal_job_title_current, ideal_salary: @user.ideal_salary, most_recent_company: @user.most_recent_company, most_recent_job_title: @user.most_recent_job_title, native_language: @user.native_language, password: @user.password, user_type: @user.user_type, years_experience: @user.years_experience }
    assert_redirected_to user_path(assigns(:user))
  end

  test "should destroy user" do
    assert_difference('User.count', -1) do
      delete :destroy, id: @user
    end

    assert_redirected_to users_path
  end
end
