require 'test_helper'

class MatchesControllerTest < ActionController::TestCase
  setup do
    @match = matches(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:matches)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create match" do
    assert_difference('Match.count') do
      post :create, match: { image_source: @match.image_source, job_title: @match.job_title, match_point_1: @match.match_point_1, match_point_2: @match.match_point_2, matchable_id: @match.matchable_id, matchable_type: @match.matchable_type, percent_match: @match.percent_match }
    end

    assert_redirected_to match_path(assigns(:match))
  end

  test "should show match" do
    get :show, id: @match
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @match
    assert_response :success
  end

  test "should update match" do
    patch :update, id: @match, match: { image_source: @match.image_source, job_title: @match.job_title, match_point_1: @match.match_point_1, match_point_2: @match.match_point_2, matchable_id: @match.matchable_id, matchable_type: @match.matchable_type, percent_match: @match.percent_match }
    assert_redirected_to match_path(assigns(:match))
  end

  test "should destroy match" do
    assert_difference('Match.count', -1) do
      delete :destroy, id: @match
    end

    assert_redirected_to matches_path
  end
end
