require 'csv'
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# company_csv = CSV.read()
# company_csv_row = company_csv[1]


CSV.foreach("./db/seed/company.csv", headers: true, header_converters: :symbol) do |row|
  company  = Company.new(row.to_hash)
  company.save
end

CSV.foreach("./db/seed/candidate.csv", headers: true, header_converters: :symbol) do |row|
  user = User.new(row.to_hash)
  user.save
end


CSV.foreach("./db/seed/candidate-matches.csv", headers: true, header_converters: :symbol) do |row|
  match_hash = row.to_hash
  match_hash[:matchable_type] = "User"
  match_hash[:matchable_id] = User.first.id
  match = Match.new(match_hash)
  match.save
end


CSV.foreach("./db/seed/company-matches.csv", headers: true, header_converters: :symbol) do |row|
  match_hash = row.to_hash
  match_hash[:matchable_type] = "Company"
  match_hash[:matchable_id] = Company.first.id
  match = Match.new(match_hash)
  match.save
end


