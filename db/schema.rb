# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140412170639) do

  create_table "companies", force: true do |t|
    t.string   "organization"
    t.string   "hiring"
    t.string   "expected_salary"
    t.string   "ideal_degree"
    t.string   "ideal_poach_company"
    t.string   "looking_for_a"
    t.string   "with"
    t.string   "native_languages"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "matches", force: true do |t|
    t.string   "matchable_type"
    t.string   "matchable_id"
    t.string   "image_source"
    t.string   "percent_match"
    t.string   "job_title"
    t.string   "match_point_1"
    t.string   "match_point_2"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "user_type"
    t.string   "email"
    t.string   "password"
    t.string   "full_name"
    t.string   "most_recent_job_title"
    t.string   "ideal_job_title_current"
    t.string   "most_recent_company"
    t.string   "ideal_salary"
    t.string   "education_cap"
    t.string   "years_experience"
    t.string   "native_language"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
