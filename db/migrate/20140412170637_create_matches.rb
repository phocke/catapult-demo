class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.string :matchable_type
      t.string :matchable_id
      t.string :image_source
      t.string :percent_match
      t.string :job_title
      t.string :match_point_1
      t.string :match_point_2

      t.timestamps
    end
  end
end
