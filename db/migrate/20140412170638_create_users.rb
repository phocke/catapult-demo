class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :user_type
      t.string :email
      t.string :password
      t.string :full_name
      t.string :most_recent_job_title
      t.string :ideal_job_title_current
      t.string :most_recent_company
      t.string :ideal_salary
      t.string :education_cap
      t.string :years_experience
      t.string :native_language

      t.timestamps
    end
  end
end
