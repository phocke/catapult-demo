class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :organization
      t.string :hiring
      t.string :expected_salary
      t.string :ideal_degree
      t.string :ideal_poach_company
      t.string :looking_for_a
      t.string :with
      t.string :native_languages

      t.timestamps
    end
  end
end
